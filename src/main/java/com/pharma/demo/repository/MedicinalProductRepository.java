package com.pharma.demo.repository;

import com.pharma.demo.entity.MedicinalProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicinalProductRepository extends JpaRepository<MedicinalProduct, Long> {
}
