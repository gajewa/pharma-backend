package com.pharma.demo.entity;

import java.util.List;

public class Packaging {

    private List<Package> packages;

    public List<Package> getPackages() {
        return packages;
    }

    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

}
